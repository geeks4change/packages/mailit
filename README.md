# MailIt

## What is?

A simple python script that sends all files from a dir to some mail address.

Built to send large heaps scanned files (autonumbered via XSane) to a collmex-conntected mail inbox. There may be other use cases.

## How install?

Put "mailit" and "isfollowup" in to your $PATH and make executable.

## How use?

- Dump files into a directory
- Add a .config file (see provided example config) to the directory
- Run `mailit`

It will log sent files in a .sent file, so in the future only new files will be sent.

## Advanced: Multi page files

- Assume you have files: scan01, scan02, scan03, scan04, scan05
- You see that scan03 & scan04 are followup pages to scan02.
- Run `isfollowup scan03 scan04` (which will add these file names to a file named .tagfile)
- Running `mailit` will then pack scan02+scan03+scan04 into one mail

